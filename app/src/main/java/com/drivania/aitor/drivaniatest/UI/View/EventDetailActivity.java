package com.drivania.aitor.drivaniatest.UI.View;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.drivania.aitor.drivaniatest.GoogleMapsAPI.LocationProvider;
import com.drivania.aitor.drivaniatest.GoogleMapsAPI.MapHttpConnection;
import com.drivania.aitor.drivaniatest.GoogleMapsAPI.PathJSONParser;
import com.drivania.aitor.drivaniatest.R;
import com.drivania.aitor.drivaniatest.Singletons.SingletonMap;
import com.drivania.aitor.drivaniatest.Util.UtilDateManagement;
import com.drivania.aitor.drivaniatest.Util.UtilStringManagement;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EventDetailActivity extends AppCompatActivity implements OnMapReadyCallback, LocationProvider.LocationCallback {

    private static final String TAG = "EventDetailActivity";

    private GoogleMap mMap;
    private LocationProvider mLocationProvider;

    private TextView eventName;
    private TextView groupName;
    private TextView eventDate;
    private TextView duration;
    private TextView venueLocation;
    private TextView venueAddress;
    private TextView venueCity;
    private TextView joinMode;

    private Button goWebSite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }else {
            Log.d(TAG, "ActionBar NULL");
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        eventName = (TextView)findViewById(R.id.txt_name);
        groupName = (TextView)findViewById(R.id.txt_name_group);
        eventDate = (TextView)findViewById(R.id.txt_date);
        duration = (TextView)findViewById(R.id.txt_duration);
        venueLocation = (TextView)findViewById(R.id.txt_name_venue);
        venueAddress = (TextView)findViewById(R.id.txt_address_venue);
        venueCity = (TextView)findViewById(R.id.txt_city_venue);
        joinMode = (TextView)findViewById(R.id.txt_join_mode);

        goWebSite = (Button)findViewById(R.id.bt_link);

        mLocationProvider = new LocationProvider(this, this, this);
        SingletonMap singletonMap = SingletonMap.getInstance();

        String event_name = UtilStringManagement.getStringWithFirstLetterUppercase(singletonMap.event.name);
        String group_name = UtilStringManagement.getStringWithFirstLetterUppercase(singletonMap.event.group.name);
        String event_date = UtilDateManagement.getDateByTimestamp(singletonMap.event.time);
        float time = (float)singletonMap.event.duration/1000/60/60;

        eventName.setText(event_name);
        groupName.setText(group_name);
        eventDate.setText(event_date);
        if (time>0){
            String event_duration = time+" h";
            duration.setText(event_duration);
        }else {
            duration.setVisibility(View.GONE);
        }

        if (singletonMap.event.venue != null){
            venueLocation.setText(singletonMap.event.venue.name);
            venueAddress.setText(singletonMap.event.venue.address_1);
            venueCity.setText(singletonMap.event.venue.city);
        }else {
            venueLocation.setVisibility(View.GONE);
            venueAddress.setVisibility(View.GONE);
            venueCity.setVisibility(View.GONE);
        }

        if (singletonMap.event.group != null){
            String mode = UtilStringManagement.getStringWithFirstLetterUppercase(singletonMap.event.group.join_mode);
            joinMode.setText(mode);
        }else {
            joinMode.setVisibility(View.GONE);
        }

        if (singletonMap.event.link != null){
            final String link = singletonMap.event.link;
            goWebSite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));
                }
            });
        }else {
            goWebSite.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mLocationProvider.connect();
    }

    @Override
    public void onStop() {
        mLocationProvider.disconnect();
        super.onStop();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1600);
        }
        mMap.setMyLocationEnabled(true);

        SingletonMap singletonMap = SingletonMap.getInstance();
        // Add a marker in Event and move the camera
        LatLng eventMark;
        if(singletonMap.event.group != null){
            eventMark = new LatLng(singletonMap.event.group.lat, singletonMap.event.group.lon);
        }else {
            //Barcelona
            eventMark = new LatLng(41.390205, 2.154007);
        }

        mMap.addMarker(new MarkerOptions().position(eventMark).title(singletonMap.event.name));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(eventMark));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(10));
    }

    @Override
    public void handleNewLocation(Location location) {

        SingletonMap singletonMap = SingletonMap.getInstance();

        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();

        if(singletonMap.event.group != null){
            LatLng myLocation = new LatLng(currentLatitude, currentLongitude);
            LatLng destination = new LatLng(singletonMap.event.group.lat, singletonMap.event.group.lon);

            float distance = getDistanceInMetersBetween2points(myLocation,destination);
            boolean walking = false;
            if (distance < 500){
                walking = true;
            }
            String url = getMapsApiDirectionsUrl(myLocation, destination, walking);

            ReadTask downloadTask = new ReadTask();
            downloadTask.execute(url);
        }
    }

    private float getDistanceInMetersBetween2points(LatLng origin, LatLng dest){
        float[] results = new float[1];
        Location.distanceBetween(origin.latitude, origin.longitude,
                dest.latitude, dest.longitude,
                results);
        return results[0];
    }

    /**
     * Build URL for GoogleMapAPI.
     * @param origin my location.
     * @param dest Location of event.
     * @return URL.
     */
    private String getMapsApiDirectionsUrl(LatLng origin, LatLng dest, boolean walking) {

        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters;
        if (walking){
            String mode = "mode=walking";
            parameters = str_origin+"&"+str_dest+"&"+sensor+"&"+mode;
        }else {
            parameters = str_origin+"&"+str_dest+"&"+sensor;
        }

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;

        return url;
    }

    /**
     * Async Task for get path between 2 points.
     */
    private class ReadTask extends AsyncTask<String, Void , String> {

        @Override
        protected String doInBackground(String... url) {
            // TODO Auto-generated method stub
            String data = "";
            try {
                MapHttpConnection http = new MapHttpConnection();
                data = http.readUr(url[0]);
            } catch (Exception e) {
                // TODO: handle exception
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            new ParserTask().execute(result);
        }

        private class ParserTask extends AsyncTask<String,Integer, List<List<HashMap<String , String >>>> {
            @Override
            protected List<List<HashMap<String, String>>> doInBackground(
                    String... jsonData) {
                // TODO Auto-generated method stub
                JSONObject jObject;
                List<List<HashMap<String, String>>> routes = null;
                try {
                    jObject = new JSONObject(jsonData[0]);
                    PathJSONParser parser = new PathJSONParser();
                    routes = parser.parse(jObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return routes;
            }

            @Override
            protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
                ArrayList<LatLng> points = null;
                PolylineOptions polyLineOptions = null;

                // traversing through routes
                for (int i = 0; i < routes.size(); i++) {
                    points = new ArrayList<LatLng>();
                    polyLineOptions = new PolylineOptions();
                    List<HashMap<String, String>> path = routes.get(i);

                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }

                    polyLineOptions.addAll(points);
                    polyLineOptions.width(4);
                    polyLineOptions.color(Color.BLUE);
                }

                mMap.addPolyline(polyLineOptions);
            }}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
