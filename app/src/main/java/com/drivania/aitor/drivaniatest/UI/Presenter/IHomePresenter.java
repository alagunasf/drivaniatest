package com.drivania.aitor.drivaniatest.UI.Presenter;

public interface IHomePresenter {

    void onDestroy();
    void onStart();
    void onStop();
    boolean isExistCode();
    boolean isExistToken();
    void verifyAuthAndFindEvents();
}
