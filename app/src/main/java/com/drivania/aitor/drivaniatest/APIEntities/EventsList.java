package com.drivania.aitor.drivaniatest.APIEntities;

import java.util.List;

public class EventsList {

    public List<Event> eventList;

    @Override
    public String toString() {
        return "EventsList{" +
                "eventList=" + eventList +
                '}';
    }
}
