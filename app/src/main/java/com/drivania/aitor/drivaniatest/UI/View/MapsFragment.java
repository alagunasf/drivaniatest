package com.drivania.aitor.drivaniatest.UI.View;

import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.drivania.aitor.drivaniatest.APIEntities.Event;
import com.drivania.aitor.drivaniatest.APIEntities.NotificationMarks;
import com.drivania.aitor.drivaniatest.GoogleMapsAPI.LocationProvider;
import com.drivania.aitor.drivaniatest.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class MapsFragment extends Fragment implements OnMapReadyCallback, LocationProvider.LocationCallback {

    private static final String TAG = "MapsFragment";

    private GoogleMap mMap;
    private LocationProvider mLocationProvider;

    public MapsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, null, false);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mLocationProvider = new LocationProvider(getActivity(), getContext(), this);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        mLocationProvider.connect();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        mLocationProvider.disconnect();
        super.onStop();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1600);
        }
        mMap.setMyLocationEnabled(true);
    }

    @Override
    public void handleNewLocation(Location location) {

        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();

        LatLng MyLocation = new LatLng(currentLatitude, currentLongitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(MyLocation));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(10));
    }

    @Subscribe
    public void onEvent(NotificationMarks notificationMarks){

        for (Event event : notificationMarks.eventList){
            if (event.group != null){
                LatLng eventMark = new LatLng(event.group.lat, event.group.lon);
                mMap.addMarker(new MarkerOptions().position(eventMark).title(event.name));
            }
        }
    }
}