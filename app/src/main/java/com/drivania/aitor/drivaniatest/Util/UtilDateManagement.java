package com.drivania.aitor.drivaniatest.Util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UtilDateManagement {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    public static String getDateByTimestamp(long timeStamp){
        Timestamp timestamp = new Timestamp(timeStamp);
        return sdf.format(timestamp);
    }
}
