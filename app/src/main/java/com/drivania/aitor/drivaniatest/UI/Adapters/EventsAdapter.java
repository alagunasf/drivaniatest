package com.drivania.aitor.drivaniatest.UI.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.percent.PercentFrameLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.drivania.aitor.drivaniatest.R;
import com.drivania.aitor.drivaniatest.APIEntities.Event;
import com.drivania.aitor.drivaniatest.Singletons.SingletonMap;
import com.drivania.aitor.drivaniatest.UI.View.EventDetailActivity;
import com.drivania.aitor.drivaniatest.Util.Constants;
import com.drivania.aitor.drivaniatest.Util.UtilDateManagement;
import com.drivania.aitor.drivaniatest.Util.UtilStringManagement;

import java.util.ArrayList;
import java.util.List;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.ViewHolder> {

    private static final String TAG = "EventsAdapter";

    private Context context;
    private List<Event> eventList;

    public EventsAdapter(Context context){
        this.context = context;
        this.eventList = new ArrayList<>();
    }

    public void addData(Event event) {
        eventList.add(event);
        notifyDataSetChanged();
    }

    public void clear() {
        eventList.clear();
        notifyDataSetChanged();
    }

    @Override
    public EventsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View itemLayoutView = LayoutInflater.from(
                viewGroup.getContext()).inflate(R.layout.row_event, viewGroup, false);

        return new ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(EventsAdapter.ViewHolder viewHolder, final int position) {
        Event event = eventList.get(position);
        viewHolder.bind(event);
        viewHolder.row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SingletonMap singletonMap = SingletonMap.getInstance();
                singletonMap.event = eventList.get(position);
                Intent intent = new Intent(context, EventDetailActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return eventList == null ? 0 : eventList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private PercentFrameLayout row;

        private TextView txtTitle;
        private TextView txtDate;
        private TextView txtMode;
        private TextView txtCircle;

        private View circle;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            try {
                row = (PercentFrameLayout) itemLayoutView.findViewById(R.id.row_event);
                txtTitle = (TextView) itemLayoutView.findViewById(R.id.title_event);
                txtDate = (TextView) itemLayoutView.findViewById(R.id.date_event);
                txtMode = (TextView) itemLayoutView.findViewById(R.id.mode_event);
                circle = itemLayoutView.findViewById(R.id.circle);
                txtCircle = (TextView) itemLayoutView.findViewById(R.id.title_circle);
            }catch(Exception e){
                Log.d(TAG, "Error en el ViewHolder EventsAdapter" + Log.getStackTraceString(e));
            }
        }

        private void bind(Event event) {

            String name = event.name;
            String date = UtilDateManagement.getDateByTimestamp(event.time);

            txtTitle.setText(name);
            txtDate.setText(date);

            if (event.group != null){
                String mode = event.group.join_mode;
                txtMode.setText(UtilStringManagement.getStringWithFirstLetterUppercase(mode));
                if (mode.equals(Constants.OPEN)){
                    txtCircle.setText("O");
                    circle.setBackgroundResource(R.drawable.circle_green);
                }else if (mode.equals(Constants.APPROVAL)){
                    txtCircle.setText("A");
                    circle.setBackgroundResource(R.drawable.circle_orange);
                }
            }
        }
    }
}
