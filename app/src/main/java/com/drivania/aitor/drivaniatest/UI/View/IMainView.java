package com.drivania.aitor.drivaniatest.UI.View;

public interface IMainView {

    void openWebView();
    void goToActivity(Class c);
    void showDialogGPS();
}
