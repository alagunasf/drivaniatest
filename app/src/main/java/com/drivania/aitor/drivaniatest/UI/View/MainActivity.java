package com.drivania.aitor.drivaniatest.UI.View;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.drivania.aitor.drivaniatest.R;
import com.drivania.aitor.drivaniatest.UI.Presenter.IHomePresenter;
import com.drivania.aitor.drivaniatest.UI.Presenter.MainPresenter;
import com.drivania.aitor.drivaniatest.Util.Constants;
import com.drivania.aitor.drivaniatest.Util.UtilPreferences;

public class MainActivity extends AppCompatActivity implements IMainView, View.OnClickListener{

    private static final String TAG = "MainActivity";

    private IHomePresenter presenter;

    private Dialog dialog;
    private WebView web;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.button).setOnClickListener(this);

        presenter = new MainPresenter(this,this);
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    public void onStop() {
        presenter.onStop();
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        presenter.verifyAuthAndFindEvents();
    }

    @Override
    public void openWebView() {

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.auth_dialog);

        web = (WebView) dialog.findViewById(R.id.webv);
        web.getSettings().setJavaScriptEnabled(true);

        String URL = Constants.URL_AUTH;
        URL +="client_id="+Constants.CLIENT_ID;
        URL +="&response_type="+Constants.RESPONSE_TYPE;
        URL +="&redirect_uri="+Constants.REDIRECT_URI;

        web.loadUrl(URL);
        web.setWebViewClient(new WebViewClient() {

            boolean authComplete = false;

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon){
                super.onPageStarted(view, url, favicon);

                if (url.contains("?submit=1")){
                    Log.d(TAG, "url start: "+url);
                    dialog.dismiss();
                    presenter.verifyAuthAndFindEvents();
                }
            }

            String authCode;

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if(url.contains("?code=") && !authComplete){
                    Uri uri = Uri.parse(url);
                    authCode = uri.getQueryParameter("code");

                    Log.d(TAG, "authCode: "+authCode);

                    authComplete = true;

                    UtilPreferences.setPreference(MainActivity.this, "Code", authCode);

                    dialog.dismiss();
                    presenter.verifyAuthAndFindEvents();
                }else if (url.contains("?submit=1")){
                    dialog.dismiss();
                    presenter.verifyAuthAndFindEvents();
                }
            }
        });

        dialog.setTitle(R.string.authorize);
        dialog.setCancelable(true);
        dialog.show();
    }

    @Override
    public void goToActivity(Class c) {
        Intent intent = new Intent(this, c);
        startActivity(intent);
    }

    @Override
    public void showDialogGPS() {
        
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setMessage(R.string.NEED_ACT_GPS);
        alertDialog.setPositiveButton(R.string.settings, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                MainActivity.this.startActivity(intent);
            }
        });
        alertDialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }
}
