package com.drivania.aitor.drivaniatest.UI.View;

public interface IEventsView {

    void showProgressDialog();
    void hideProgressDialog();
    void goToActivity(Class c);
}
