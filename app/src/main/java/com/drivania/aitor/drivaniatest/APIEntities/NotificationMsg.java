package com.drivania.aitor.drivaniatest.APIEntities;

import com.drivania.aitor.drivaniatest.UI.Adapters.EventsAdapter;

/**
 * Object to pass at onEvent functions.
 */
public class NotificationMsg {

    public EventsAdapter eventsAdapter;
    public String msg;

    public NotificationMsg(EventsAdapter eventsAdapter, String msg){
        this.eventsAdapter = eventsAdapter;
        this.msg = msg;
    }
}
