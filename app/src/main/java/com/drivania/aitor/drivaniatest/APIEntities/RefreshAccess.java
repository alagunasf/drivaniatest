package com.drivania.aitor.drivaniatest.APIEntities;

public class RefreshAccess {

    public String refresh_token;
    public String grant_type;
    public String client_id;
    public String client_secret;
}
