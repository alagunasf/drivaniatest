package com.drivania.aitor.drivaniatest.UI.Model;

import android.content.Context;

import com.drivania.aitor.drivaniatest.Request.RetrofitCalls;
import com.drivania.aitor.drivaniatest.UI.Adapters.EventsAdapter;
import com.drivania.aitor.drivaniatest.Util.Constants;
import com.drivania.aitor.drivaniatest.Util.UtilPreferences;

public class MainInteractor implements IMainInteractor {

    private static final String TAG = "MainInteractor";

    @Override
    public void getToken(Context context) {
        String code = UtilPreferences.getPreference(context,"Code");
        RetrofitCalls.getToken(context, Constants.URL_ACCESS_TOKEN, code);
    }

    @Override
    public void getRefreshToken(Context context, EventsAdapter eventsAdapter) {
        String refresh = UtilPreferences.getPreference(context,"Refresh");
        RetrofitCalls.getRefreshToken(context, Constants.URL_ACCESS_TOKEN, refresh, eventsAdapter);
    }
}
