package com.drivania.aitor.drivaniatest.UI.View;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.drivania.aitor.drivaniatest.R;
import com.drivania.aitor.drivaniatest.UI.Adapters.EventsAdapter;
import com.drivania.aitor.drivaniatest.UI.Presenter.EventsPresenter;
import com.drivania.aitor.drivaniatest.UI.Presenter.IEventsPresenter;

public class EventsActivity extends AppCompatActivity implements IEventsView{

    private static final String TAG = "EventsActivity";

    private IEventsPresenter presenter;
    private TabFragment tabFragment;
    private ViewPager mViewPager;
    private TabLayout tabLayout;
    private ProgressDialog pDlg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);

        //Activity, Context and Interface.
        this.presenter = new EventsPresenter(this, this, this);
        this.tabFragment = new TabFragment(getSupportFragmentManager(),new EventsAdapter(this), this);

        mViewPager = (ViewPager) findViewById(R.id.container);
        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);

        presenter.onCreate();
        presenter.waitingForEvents();
        mViewPager.setAdapter(tabFragment);
        tabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onStop() {
        presenter.onStop();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showProgressDialog() {

        pDlg = new ProgressDialog(this);
        pDlg.setMessage("Loading");
        pDlg.setProgressDrawable(this.getWallpaper());
        pDlg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDlg.setCancelable(false);
        pDlg.show();
    }

    @Override
    public void hideProgressDialog() {
        pDlg.dismiss();
    }

    @Override
    public void goToActivity(Class c) {
        Intent intent = new Intent(this, c);
        startActivity(intent);
    }
}
