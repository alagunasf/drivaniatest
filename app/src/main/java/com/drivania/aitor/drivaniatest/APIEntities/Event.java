package com.drivania.aitor.drivaniatest.APIEntities;

/**
 * Event object. This information come to the WS.
 * Ready for parse with GSON.
 */
public class Event {

    public long created;
    public long duration;
    public String id;
    public String name;
    public long resvp_limit;
    public String status;
    public long time;
    public long updated;
    public long utc_offset;
    public long waitlist_count;
    public long yes_rsvp_count;
    public Venue venue;
    public Group group;
    public String link;
    public String description;
    public String visibility;

    @Override
    public String toString() {
        return "Event{" +
                "created=" + created +
                ", duration=" + duration +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", resvp_limit=" + resvp_limit +
                ", status='" + status + '\'' +
                ", time=" + time +
                ", updated=" + updated +
                ", utc_offset=" + utc_offset +
                ", waitlist_count=" + waitlist_count +
                ", yes_rsvp_count=" + yes_rsvp_count +
                ", venue=" + venue +
                ", group=" + group +
                ", link='" + link + '\'' +
                ", description='" + description + '\'' +
                ", visibility='" + visibility + '\'' +
                '}';
    }
}
