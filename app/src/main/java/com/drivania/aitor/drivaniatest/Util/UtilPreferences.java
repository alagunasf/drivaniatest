package com.drivania.aitor.drivaniatest.Util;

import android.content.Context;
import android.content.SharedPreferences;

public class UtilPreferences {

    private static SharedPreferences sharedPreferences;
    private static String PREFS_NAME = "AppPref";

    public static boolean setPreference(Context context, String key, String value) {

        sharedPreferences = context.getSharedPreferences(PREFS_NAME, context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);

        return editor.commit();
    }

    public static String getPreference(Context context, String key) {

        sharedPreferences = context.getSharedPreferences(PREFS_NAME, context.MODE_PRIVATE);
        String value = sharedPreferences.getString(key, "");

        return value;
    }
}
