package com.drivania.aitor.drivaniatest.UI.Presenter;

import android.content.Context;
import android.location.LocationManager;
import android.util.Log;
import android.widget.Toast;

import com.drivania.aitor.drivaniatest.UI.Model.IMainInteractor;
import com.drivania.aitor.drivaniatest.UI.Model.MainInteractor;
import com.drivania.aitor.drivaniatest.UI.View.EventsActivity;
import com.drivania.aitor.drivaniatest.UI.View.IMainView;
import com.drivania.aitor.drivaniatest.Util.Constants;
import com.drivania.aitor.drivaniatest.Util.UtilPreferences;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import static android.content.Context.LOCATION_SERVICE;

public class MainPresenter implements IHomePresenter{

    private static final String TAG = "MainPresenter";

    private static final int NOT_EXIST_CODE = 1;
    private static final int NOT_EXIST_TOKEN = 2;
    private static final int FIND_EVENTS = 3;

    private IMainView iMainView;
    private IMainInteractor iMainInteractor;
    private Context context;
    private LocationManager mLocationManager;

    public MainPresenter(Context context, IMainView iMainView){

        this.context = context;
        this.iMainView = iMainView;
        this.iMainInteractor = new MainInteractor();
        this.mLocationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
    }

    @Override
    public void onStart() {
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDestroy() {
        iMainView = null;
    }

    @Override
    public boolean isExistCode() {
        String code = UtilPreferences.getPreference(context,"Code");
        Log.d(TAG, "CODE: "+code);
        if (code.isEmpty()){
            return false;
        }
        return true;
    }

    @Override
    public boolean isExistToken() {
        String token = UtilPreferences.getPreference(context,"Token");
        Log.d(TAG, "TOKEN: "+token);
        if (token.isEmpty()){
            return false;
        }
        return true;
    }

    @Override
    public void verifyAuthAndFindEvents() {
        if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            iMainView.showDialogGPS();
        }else {
            switch (getStep()){
                case NOT_EXIST_CODE:
                    Log.d(TAG, "STEP 1");
                    iMainView.openWebView();
                    break;
                case NOT_EXIST_TOKEN:
                    Log.d(TAG, "STEP 2");
                    iMainInteractor.getToken(context);
                    break;
                case FIND_EVENTS:
                    Log.d(TAG, "STEP 3");
                    iMainView.goToActivity(EventsActivity.class);
                    break;
            }
        }
    }

    @Subscribe
    public void onEvent(String msg){
        Log.d(TAG, "onEvent msg: "+msg);
        switch (msg){
            case Constants.GET_TOKEN_CORRECT:
                verifyAuthAndFindEvents();
                break;
            case Constants.GET_TOKEN_INCORRECT:
                Toast.makeText(context, "GET_TOKEN INCORRECT", Toast.LENGTH_SHORT).show();
                break;
            case Constants.GET_REFRESH_TOKEN_INCORRECT:
                Toast.makeText(context, "REFRESH_TOKEN INCORRECT", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    /**
     * First check if exist CODE and later check if exist TOKEN.
     * If we have CODE and TOKEN, we can find the events.
     * @return Step
     */
    private int getStep(){

        if (!isExistCode()){
            return NOT_EXIST_CODE;
        }
        if (!isExistToken()){
            return NOT_EXIST_TOKEN;
        }
        return FIND_EVENTS;
    }
}