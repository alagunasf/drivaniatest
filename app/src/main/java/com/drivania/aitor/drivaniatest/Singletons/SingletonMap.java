package com.drivania.aitor.drivaniatest.Singletons;

import com.drivania.aitor.drivaniatest.APIEntities.Event;

import java.util.ArrayList;
import java.util.List;

public class SingletonMap {

    private static SingletonMap uniqInstance;
    public List<Event> eventList;
    public Event event;
    public double currentLatitude;
    public double currentLongitude;

    public SingletonMap() {

        this.eventList = new ArrayList<>();
        this.event = new Event();
        this.currentLatitude = 0;
        this.currentLongitude = 0;
    }

    public static synchronized SingletonMap getInstance() {

        if(uniqInstance == null){
            uniqInstance = new SingletonMap();
        }

        return uniqInstance;
    }
}
