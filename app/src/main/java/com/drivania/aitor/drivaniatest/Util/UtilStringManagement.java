package com.drivania.aitor.drivaniatest.Util;

public class UtilStringManagement {

    public static String getStringWithFirstLetterUppercase(String msg){
        return msg.substring(0, 1).toUpperCase() + msg.substring(1);
    }
}
