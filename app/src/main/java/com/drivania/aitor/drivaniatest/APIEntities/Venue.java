package com.drivania.aitor.drivaniatest.APIEntities;

public class Venue {

    public int id;
    public String name;
    public double lat;
    public double lon;
    public boolean repinned;
    public String address_1;
    public String city;
    public String country;
    public String localized_country_name;

    @Override
    public String toString() {
        return "Venue{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lat=" + lat +
                ", lon=" + lon +
                ", repinned=" + repinned +
                ", address_1='" + address_1 + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", localized_country_name='" + localized_country_name + '\'' +
                '}';
    }
}
