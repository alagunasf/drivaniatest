package com.drivania.aitor.drivaniatest.UI.Model;

import android.content.Context;

import com.drivania.aitor.drivaniatest.UI.Adapters.EventsAdapter;

public interface IEventsInteractor {

    void getEvents(Context context, EventsAdapter eventsAdapter);
    void getRefreshToken(Context context, EventsAdapter eventsAdapter);
}
