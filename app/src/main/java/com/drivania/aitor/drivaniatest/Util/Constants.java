package com.drivania.aitor.drivaniatest.Util;


public class Constants {

    public static final String CLIENT_ID = "bof94q0irv2spqrnpctj4om9m5";
    public static final String CLIENT_SECRET = "cheaqnqhnqn1dj35fd4u0q0csg";
    public static final String REDIRECT_URI = "http://es.drivania.com/";
    public static final String GRANT_TYPE = "authorization_code";
    public static final String GRANT_TYPE_REFRESH = "refresh_token";
    public static final String RESPONSE_TYPE = "code";

    public static final String URL_AUTH = "https://secure.meetup.com/oauth2/authorize?";
    public static final String URL_ACCESS_TOKEN = "https://secure.meetup.com/";
    public static final String URL_API_MEETUP = "https://api.meetup.com";

    public static String URL_RETROFIT_CALL = "";

    public static final String GET_TOKEN_CORRECT = "GET_TOKEN_CORRECT";
    public static final String GET_TOKEN_INCORRECT = "GET_TOKEN_INCORRECT";

    public static final String GET_REFRESH_TOKEN_CORRECT = "GET_REFRESH_TOKEN_CORRECT";
    public static final String GET_REFRESH_TOKEN_INCORRECT = "GET_REFRESH_TOKEN_INCORRECT";

    public static final String GET_EVENTS_CORRECT = "GET_EVENTS_CORRECT";
    public static final String GET_EVENTS_INCORRECT = "GET_EVENTS_INCORRECT";
    public static final String GET_EVENTS_INCORRECT_NOT_FOUND = "GET_EVENTS_INCORRECT_NOT_FOUND";
    public static final String GET_EVENTS_INCORRECT_TOKEN = "GET_EVENTS_INCORRECT_TOKEN";
    public static final String READY_FOR_FIND_EVENTS = "READY_FOR_FIND_EVENTS";

    public static final long TEN_SECONDS = 10 * 1000;
    public static final long ONE_SECOND  = 1 * 1000;

    public static final String EVENTS = "EVENTS";
    public static final String MAP = "MAP";

    public static final String OPEN = "open";
    public static final String APPROVAL = "approval";
}
