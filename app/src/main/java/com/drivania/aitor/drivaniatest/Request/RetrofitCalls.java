package com.drivania.aitor.drivaniatest.Request;

import android.content.Context;
import android.util.Log;

import com.drivania.aitor.drivaniatest.APIEntities.Access;
import com.drivania.aitor.drivaniatest.APIEntities.Event;
import com.drivania.aitor.drivaniatest.APIEntities.NotificationMarks;
import com.drivania.aitor.drivaniatest.APIEntities.NotificationMsg;
import com.drivania.aitor.drivaniatest.APIEntities.RefreshAccess;
import com.drivania.aitor.drivaniatest.APIEntities.Token;
import com.drivania.aitor.drivaniatest.Singletons.SingletonMap;
import com.drivania.aitor.drivaniatest.UI.Adapters.EventsAdapter;
import com.drivania.aitor.drivaniatest.Util.Constants;
import com.drivania.aitor.drivaniatest.Util.UtilPreferences;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Class to use Retrofit for API's calls.
 */
public class RetrofitCalls {

    public static final String TAG = "RetrofitCalls";

    /**
     * Get Access Token and Refresh Token.
     * @param context Context.
     * @param URL API URL.
     * @param code Code for get token.
     */
    public static void getToken(final Context context, String URL, final String code){

        Constants.URL_RETROFIT_CALL = URL;
        MeetUpAPI serviceRx = MeetUpAPI.retrofit.create(MeetUpAPI.class);

        Access access = new Access();
        access.code = code;
        access.redirect_uri = Constants.REDIRECT_URI;
        access.grant_type = Constants.GRANT_TYPE;
        access.client_id = Constants.CLIENT_ID;
        access.client_secret = Constants.CLIENT_SECRET;

        Call<Token> call = serviceRx.getToken(access.client_id, access.client_secret, access.code,access.grant_type, access.redirect_uri);

        call.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                Token token =  response.body();
                UtilPreferences.setPreference(context, "Token", token.access_token);
                UtilPreferences.setPreference(context, "Refresh", token.refresh_token);
                EventBus.getDefault().post(Constants.GET_TOKEN_CORRECT);
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                EventBus.getDefault().post(Constants.GET_TOKEN_INCORRECT);
                Log.d(TAG, "onFailure: "+t.toString());
            }
        });
    }

    /**
     * Get Events of the MeetUp API.
     * @param context Context.
     * @param eventsAdapter Adapter of RecyclerView for notify DataSet has changed.
     */
    public static void getEvents(final Context context, final EventsAdapter eventsAdapter){

        Constants.URL_RETROFIT_CALL = Constants.URL_API_MEETUP;
        MeetUpAPI serviceRx = MeetUpAPI.retrofit.create(MeetUpAPI.class);
        String token = UtilPreferences.getPreference(context, "Token");
        SingletonMap singletonMap = SingletonMap.getInstance();
        Call<List<Event>> call = serviceRx.getEvents(("Bearer "+token),
                singletonMap.currentLatitude,singletonMap.currentLongitude);

        call.enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                Log.d(TAG, "onResponse: "+response.code());

                if (response.code() == 200){
                    List<Event> eventsList = response.body();
                    if (eventsList == null){
                        Log.d(TAG, "eventList is null");
                    }else {
                        EventBus.getDefault().post(Constants.GET_EVENTS_CORRECT);
                        for (Event event : eventsList){
                            eventsAdapter.addData(event);
                        }
                        EventBus.getDefault().post(new NotificationMarks(eventsList));
                    }
                }else if (response.code() == 404){
                    EventBus.getDefault().post(Constants.GET_EVENTS_INCORRECT_NOT_FOUND);
                }else {
                    //Code 401 - Unauthorized
                    EventBus.getDefault().post(new NotificationMsg(eventsAdapter, Constants.GET_EVENTS_INCORRECT_TOKEN));
                }
            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.toString());
                EventBus.getDefault().post(Constants.GET_EVENTS_INCORRECT);
            }
        });
    }

    /**
     * Get Refresh Token when the Access Token has expired.
     * @param context Context.
     * @param URL API URL.
     * @param code Refresh Token.
     * @param eventsAdapter Adapter of RecyclerView for notify DataSet has changed.
     */
    public static void getRefreshToken(final Context context, String URL, final String code, final EventsAdapter eventsAdapter){

        Constants.URL_RETROFIT_CALL = URL;
        MeetUpAPI serviceRx = MeetUpAPI.retrofit.create(MeetUpAPI.class);

        RefreshAccess refreshAccess = new RefreshAccess();
        refreshAccess.refresh_token = code;
        refreshAccess.grant_type = Constants.GRANT_TYPE_REFRESH;
        refreshAccess.client_id = Constants.CLIENT_ID;
        refreshAccess.client_secret = Constants.CLIENT_SECRET;


        Call<Token> call = serviceRx.getRefreshToken(refreshAccess.client_id, refreshAccess.client_secret, refreshAccess.refresh_token,refreshAccess.grant_type);

        call.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                Token token =  response.body();
                UtilPreferences.setPreference(context, "Token", token.access_token);
                UtilPreferences.setPreference(context, "Refresh", token.refresh_token);
                EventBus.getDefault().post(new NotificationMsg(eventsAdapter, Constants.GET_REFRESH_TOKEN_CORRECT));
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.toString());
                EventBus.getDefault().post(Constants.GET_REFRESH_TOKEN_INCORRECT);
            }
        });
    }
}