package com.drivania.aitor.drivaniatest.APIEntities;

public class Group {

    public long created;
    public String name;
    public long id;
    public String join_mode;
    public double lat;
    public double lon;
    public String urlname;
    public String who;

    @Override
    public String toString() {
        return "Group{" +
                "created=" + created +
                ", name='" + name + '\'' +
                ", id=" + id +
                ", join_mode='" + join_mode + '\'' +
                ", lat=" + lat +
                ", lon=" + lon +
                ", urlname='" + urlname + '\'' +
                ", who='" + who + '\'' +
                '}';
    }
}
