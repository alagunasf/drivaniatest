package com.drivania.aitor.drivaniatest.UI.Model;

import android.content.Context;

import com.drivania.aitor.drivaniatest.Request.RetrofitCalls;
import com.drivania.aitor.drivaniatest.UI.Adapters.EventsAdapter;
import com.drivania.aitor.drivaniatest.Util.Constants;
import com.drivania.aitor.drivaniatest.Util.UtilPreferences;

public class EventsInteractor implements IEventsInteractor{

    private static final String TAG = "EventsInteractor";

    @Override
    public void getRefreshToken(Context context, EventsAdapter eventsAdapter) {
        String refresh = UtilPreferences.getPreference(context,"Refresh");
        RetrofitCalls.getRefreshToken(context, Constants.URL_ACCESS_TOKEN, refresh, eventsAdapter);
    }

    @Override
    public void getEvents(Context context, EventsAdapter eventsAdapter) {
        RetrofitCalls.getEvents(context, eventsAdapter);
    }
}
