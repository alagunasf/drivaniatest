package com.drivania.aitor.drivaniatest.UI.View;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.drivania.aitor.drivaniatest.R;
import com.drivania.aitor.drivaniatest.Render.DividerItemDecoration;
import com.drivania.aitor.drivaniatest.Request.RetrofitCalls;
import com.drivania.aitor.drivaniatest.UI.Adapters.EventsAdapter;
import com.drivania.aitor.drivaniatest.Util.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class EventsFragment extends Fragment {

    private static final String TAG = "EventsFragment";

    private EventsAdapter eventsAdapter;

    public static EventsFragment newInstance(EventsAdapter eventsAdapter) {
        EventsFragment fragment = new EventsFragment();
        fragment.setAdapter(eventsAdapter);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.tab_events, container, false);

        RecyclerView mRecyclerView = (RecyclerView) (rootView != null ? rootView.findViewById(R.id.recyclerViewEvents) : null);

        if (mRecyclerView != null) {
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
            mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(eventsAdapter);
        }else{
            Log.d(TAG, "mRecyclerView null");
        }
        return rootView;
    }

    private void setAdapter(EventsAdapter eventsAdapter){
        this.eventsAdapter = eventsAdapter;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(String msg){
        if (msg.equals(Constants.READY_FOR_FIND_EVENTS)){
            RetrofitCalls.getEvents(getActivity().getApplicationContext(),eventsAdapter);
        }
    }
}
