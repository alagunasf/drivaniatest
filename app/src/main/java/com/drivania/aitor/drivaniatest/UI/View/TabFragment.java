package com.drivania.aitor.drivaniatest.UI.View;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.drivania.aitor.drivaniatest.UI.Adapters.EventsAdapter;
import com.drivania.aitor.drivaniatest.Util.Constants;

public class TabFragment extends FragmentPagerAdapter {

    private Activity activity;
    public EventsAdapter eventsAdapter;
    public TabFragment(FragmentManager fm, EventsAdapter eventsAdapter, Activity activity) {
        super(fm);
        this.eventsAdapter = eventsAdapter;
        this.activity = activity;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a fragment to open.
        switch (position) {
            case 0:
                FragmentTransaction ft = activity.getFragmentManager().beginTransaction();
                EventsFragment fragment = EventsFragment.newInstance(eventsAdapter);
                ft.commit();
                return fragment;
            default:
                return new MapsFragment();
        }
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return Constants.EVENTS;
            case 1:
                return Constants.MAP;
        }
        return null;
    }
}
