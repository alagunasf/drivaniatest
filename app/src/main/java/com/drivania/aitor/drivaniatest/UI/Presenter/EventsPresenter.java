package com.drivania.aitor.drivaniatest.UI.Presenter;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.drivania.aitor.drivaniatest.APIEntities.NotificationMsg;
import com.drivania.aitor.drivaniatest.GoogleMapsAPI.LocationProvider;
import com.drivania.aitor.drivaniatest.Singletons.SingletonMap;
import com.drivania.aitor.drivaniatest.UI.Model.EventsInteractor;
import com.drivania.aitor.drivaniatest.UI.Model.IEventsInteractor;
import com.drivania.aitor.drivaniatest.UI.View.IEventsView;
import com.drivania.aitor.drivaniatest.UI.View.MainActivity;
import com.drivania.aitor.drivaniatest.Util.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class EventsPresenter implements IEventsPresenter, LocationProvider.LocationCallback{

    private static final String TAG = "EventsPresenter";

    private IEventsView iEventsView;
    private IEventsInteractor iEventsInteractor;
    private Context context;
    private Activity activity;
    private LocationProvider mLocationProvider;

    public EventsPresenter(Activity activity, Context context, IEventsView iEventsView) {
        this.context = context;
        this.iEventsView = iEventsView;
        this.iEventsInteractor = new EventsInteractor();
        this.activity = activity;
    }

    @Override
    public void waitingForEvents() {
        iEventsView.showProgressDialog();
    }

    @Override
    public void onCreate() {
        mLocationProvider = new LocationProvider(activity, context, this);
    }

    @Override
    public void onStart() {
        mLocationProvider.connect();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        mLocationProvider.disconnect();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDestroy() {
        iEventsView = null;
    }

    @Subscribe
    public void onEvent(String msg) {
        switch (msg) {
            case Constants.GET_REFRESH_TOKEN_INCORRECT:
                Toast.makeText(context, "REFRESH_TOKEN INCORRECT", Toast.LENGTH_SHORT).show();
                break;
            case Constants.GET_EVENTS_CORRECT:
                iEventsView.hideProgressDialog();
                break;
            case Constants.GET_EVENTS_INCORRECT:
                iEventsView.goToActivity(MainActivity.class);
                Toast.makeText(context, "Try later.", Toast.LENGTH_SHORT).show();
                break;
            case Constants.GET_EVENTS_INCORRECT_NOT_FOUND:
                iEventsView.goToActivity(MainActivity.class);
                Toast.makeText(context, "404 - Not Found. Try later.", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "404 - Not Found");
                break;
        }
    }

    @Subscribe
    public void onEvent(NotificationMsg notificationMsg){
        if (notificationMsg.msg.equals(Constants.GET_REFRESH_TOKEN_CORRECT)){
            Log.d(TAG, "Refresh Token OK");
            iEventsInteractor.getEvents(context, notificationMsg.eventsAdapter);
        }else if (notificationMsg.msg.equals(Constants.GET_EVENTS_INCORRECT_TOKEN)){
            Log.d(TAG, "401 - Unauthorized");
            iEventsInteractor.getRefreshToken(context, notificationMsg.eventsAdapter);
        }
    }

    @Override
    public void handleNewLocation(Location location) {
        Log.d(TAG, location.toString());

        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();

        SingletonMap singletonMap = SingletonMap.getInstance();
        singletonMap.currentLatitude = currentLatitude;
        singletonMap.currentLongitude = currentLongitude;
        EventBus.getDefault().post(Constants.READY_FOR_FIND_EVENTS);
    }
}