package com.drivania.aitor.drivaniatest.APIEntities;

public class Token {

    public String access_token;
    public String refresh_token;
    public String token_type;
    public String expires_in;

    @Override
    public String toString() {
        return "Token{" +
                "access_token='" + access_token + '\'' +
                ", refresh_token='" + refresh_token + '\'' +
                ", token_type='" + token_type + '\'' +
                ", expires_in='" + expires_in + '\'' +
                '}';
    }
}
