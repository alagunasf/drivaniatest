package com.drivania.aitor.drivaniatest.UI;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;


import com.drivania.aitor.drivaniatest.R;
import com.drivania.aitor.drivaniatest.UI.View.MainActivity;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends Activity  {

    private static final String TAG = "SplashActivity";
    private static final long SPLASH_SCREEN_DELAY = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    protected void onResume() {
        super.onResume();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                // Close the activity so the user won't able to go back this
                // activity pressing Back button
                finish();
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
            }
        };

        // Simulate a long loading process on application startup.
        Timer timer = new Timer();
        timer.schedule(task, SPLASH_SCREEN_DELAY);
    }
}