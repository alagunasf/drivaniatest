package com.drivania.aitor.drivaniatest.Request;

import com.drivania.aitor.drivaniatest.APIEntities.Event;
import com.drivania.aitor.drivaniatest.APIEntities.Token;
import com.drivania.aitor.drivaniatest.Util.Constants;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Retrofit configuration and calls.
 * Using OkHttp for Log.
 */
public interface MeetUpAPI {

    HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
    OkHttpClient okHttpClient = new OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor).build();

    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Constants.URL_RETROFIT_CALL)
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build();

    @POST("oauth2/access")
    @FormUrlEncoded
    Call<Token> getToken(@Field("client_id") String client_id,
                         @Field("client_secret") String client_secret,
                         @Field("code") String code,
                         @Field("grant_type") String grant_type,
                         @Field("redirect_uri") String redirect_uri);

    @POST("oauth2/access")
    @FormUrlEncoded
    Call<Token> getRefreshToken(@Field("client_id") String client_id,
                         @Field("client_secret") String client_secret,
                         @Field("refresh_token") String refresh_token,
                         @Field("grant_type") String grant_type);

    @GET("/find/events")
    Call<List<Event>> getEvents(@Header("Authorization") String authorization,
                                @Query("lat") double lat,
                                @Query("lon") double lon);
}
