package com.drivania.aitor.drivaniatest.UI.Presenter;

public interface IEventsPresenter {

    void waitingForEvents();
    void onCreate();
    void onStart();
    void onStop();
    void onDestroy();
}
