package com.drivania.aitor.drivaniatest.UI.Model;

import android.content.Context;

import com.drivania.aitor.drivaniatest.UI.Adapters.EventsAdapter;

import rx.Subscription;

public interface IMainInteractor {

    void getToken(Context context);
    void getRefreshToken(Context context, EventsAdapter eventsAdapter);
}
